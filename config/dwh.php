<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Settings for Data Warehouse
    |--------------------------------------------------------------------------
    |
    | Configurations for Data Warehouse
    |
    */
    'file_mode' => env('DWH_FILE_MODE', 'json'),
    'base_dir' => env('DWH_BASE_DIR', '/salesforce/'),
];
