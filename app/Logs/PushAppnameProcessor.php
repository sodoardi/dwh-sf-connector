<?php

namespace App\Logs;

use Monolog\Handler\ProcessableHandlerInterface;

class PushAppnameProcessor
{
    /**
     * @param mixed $logger
     */
    public function __invoke($logger): void
    {
        foreach ($logger->getHandlers() as $handler) {
            if ($handler instanceof ProcessableHandlerInterface) {
                $handler->pushProcessor(new AppnameProcessor());
            }
        }
    }
}
