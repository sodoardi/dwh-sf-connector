<?php

namespace App\Logs;

use Monolog\Processor\ProcessorInterface;

class AppnameProcessor implements ProcessorInterface
{
    /**
     * @var string $app
     */
    private static $app;

    public function __construct()
    {
        self::$app = (string) config('app.name');
    }

    public function __invoke(array $record): array
    {
        $record['extra']['app'] = self::$app;

        return $record;
    }
}
