<?php

namespace App\Logs;

use Monolog\Handler\ProcessableHandlerInterface;
use Monolog\Processor\HostnameProcessor;

class PushHostnameProcessor
{
    /**
     * @param mixed $logger
     */
    public function __invoke($logger): void
    {
        foreach ($logger->getHandlers() as $handler) {
            if ($handler instanceof ProcessableHandlerInterface) {
                $handler->pushProcessor(new HostnameProcessor());
            }
        }
    }
}
