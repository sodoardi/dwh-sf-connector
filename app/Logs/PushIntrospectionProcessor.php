<?php

namespace App\Logs;

use Monolog\Handler\ProcessableHandlerInterface;
use Monolog\Logger;
use Monolog\Processor\IntrospectionProcessor;

class PushIntrospectionProcessor
{
    /**
     * @param mixed $logger
     */
    public function __invoke($logger): void
    {
        foreach ($logger->getHandlers() as $handler) {
            if ($handler instanceof ProcessableHandlerInterface) {
                $handler->pushProcessor(new IntrospectionProcessor(Logger::DEBUG, ['Illuminate\\']));
            }
        }
    }
}
