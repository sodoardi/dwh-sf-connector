<?php

namespace App\Logs;

use DateTimeInterface;
use Monolog\Formatter\JsonFormatter;

class LogzIoFormatter extends JsonFormatter
{
    /**
     * Datetime format for Logz.io
     * @see https://support.logz.io/hc/en-us/articles/210206885
     */
    protected const DATETIME_FORMAT = 'c';

    public function __construct(int $batchMode = self::BATCH_MODE_NEWLINES, bool $appendNewline = true)
    {
        parent::__construct($batchMode, $appendNewline);
    }

    /**
     * Appends the '@timestamp' parameter for Logz.io.
     *
     * @param array $record
     * @return string
     *
     * @link https://support.logz.io/hc/en-us/articles/210206885
     * @see \Monolog\Formatter\JsonFormatter::format()
     */
    public function format(array $record): string
    {
        if (isset($record['datetime']) && ($record['datetime'] instanceof DateTimeInterface)) {
            $record['@timestamp'] = $record['datetime']->format(self::DATETIME_FORMAT);
            unset($record['datetime']);
        }

        /**
         * @var callable $filter
         */
        $filter = 'self::filterValues';
        if (isset($record['context'])) {
            $record['context'] = array_filter((array) $record['context'], $filter);
        }

        if (isset($record['extra'])) {
            $record['extra'] = array_filter((array) $record['extra'], $filter);
        }

        return parent::format($record);
    }

    /**
     * @description Logz.io does not allow [null] or [""] as context/extra.
     * @param mixed $value
     * @return bool
     */
    protected static function filterValues($value): bool
    {
        return isset($value) && $value !== '';
    }
}
