<?php

namespace App\Exceptions;

use Exception;

class CannotReopenFile extends Exception
{
}
