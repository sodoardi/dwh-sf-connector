<?php

namespace App\Services;

interface FileWriterContract
{
    public const FAILED_TO_OPEN_MESSAGE = 'Path is a directory or not writable';
    public const CANNOT_REOPEN_MESSAGE = 'Tried to reopen closed file';
    public const NO_CONTENT_MESSAGE = 'Tried to close file without content';
    public const DISK = 'local';

    public function __construct();
    public function appendRecords(array $records): void;
    public function appendRecord(array $record): void;
    public function finishFile(): FileInfo;
}
