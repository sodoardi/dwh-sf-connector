<?php

namespace App\Services;

use App\Exceptions\FailedToOpenFile;
use App\Exceptions\NoFileContent;
use Illuminate\Support\Facades\Storage;

abstract class BaseFileWriter implements FileWriterContract
{
    /**
     * @var resource
     */
    protected $fileHandler;
    /**
     * @var bool
     */
    protected $isClosed = false;
    /**
     * @var string
     */
    protected $fileName;
    /**
     * @var string
     */
    protected $fullPath;
    private $disk;
    private $basePath;

    public function __construct()
    {
        $this->disk = self::DISK;
        $this->basePath = config('dwh.base_dir');
        $this->fullPath = $this->basePath.$this->fileName;
    }

    /**
     * @param array $records
     */
    public function appendRecords(array $records): void
    {
        foreach ($records as $record) {
            $this->appendRecord($record);
        }
    }

    /**
     * @return FileInfo
     * @throws NoFileContent
     */
    public function finishFile(): FileInfo
    {
        if (!is_resource($this->fileHandler)) {
            throw new NoFileContent('[DWH] '.self::NO_CONTENT_MESSAGE.' '.self::class);
        }

        $this->finalizeFile();
        $this->closeFile();
        return new FileInfo($this->disk, $this->basePath, $this->fileName);
    }

    /**
     * @throws FailedToOpenFile
     */
    protected function openFile(): void
    {
        Storage::disk($this->disk)->makeDirectory($this->basePath);
        $fileHandler = fopen(Storage::disk($this->disk)->path($this->fullPath), 'w');
        if ($fileHandler === false) {
            throw new FailedToOpenFile('[DWH] '.self::FAILED_TO_OPEN_MESSAGE.' '.self::class);
        }
        $this->fileHandler = $fileHandler;
    }

    protected function closeFile(): void
    {
        $this->isClosed = true;
        fclose($this->fileHandler);
    }

    protected function finalizeFile(): void
    {
        return;
    }
}
