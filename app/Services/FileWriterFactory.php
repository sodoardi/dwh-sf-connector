<?php

namespace App\Services;

class FileWriterFactory
{
    private const ERROR_MESSAGE = 'Invalid type passed';

    public function getFileWriter(string $type): FileWriterContract
    {
        switch ($type) {
            case 'json':
                return new JsonFileWriter();
            case 'csv':
                return new CsvFileWriter();
        }

        throw new \InvalidArgumentException(self::class . ' - ' . self::ERROR_MESSAGE);
    }
}
