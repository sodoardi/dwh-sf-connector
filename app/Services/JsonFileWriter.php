<?php

namespace App\Services;

use App\Exceptions\CannotReopenFile;
use App\Exceptions\FailedToOpenFile;
use Illuminate\Support\Str;

class JsonFileWriter extends BaseFileWriter
{
    private const SUFFIX = '.json';

    public function __construct()
    {
        $this->fileName = Str::uuid()->toString() . self::SUFFIX;
        parent::__construct();
    }

    /**
     * @param array $record
     * @throws CannotReopenFile
     * @throws FailedToOpenFile
     */
    public function appendRecord(array $record): void
    {
        if ($this->isClosed) {
            throw new CannotReopenFile('[DWH] ' . self::CANNOT_REOPEN_MESSAGE . ' ' . self::class);
        }

        if (!is_resource($this->fileHandler)) {
            $this->initializeFile();
        }

        fwrite($this->fileHandler, json_encode($record) . ',');
    }

    protected function finalizeFile(): void
    {
        //remove last comma
        fseek($this->fileHandler, -1, SEEK_END);
        fwrite($this->fileHandler, ']');
    }

    /**
     * @throws FailedToOpenFile
     */
    private function initializeFile(): void
    {
        $this->openFile();
        fwrite($this->fileHandler, '[');
    }
}
