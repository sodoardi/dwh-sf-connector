<?php

namespace App\Services;

use App\Exceptions\CannotReopenFile;
use App\Exceptions\FailedToOpenFile;
use Illuminate\Support\Str;

class CsvFileWriter extends BaseFileWriter
{
    private const DELIMITER = ';';
    private const SUFFIX = '.csv';

    public function __construct()
    {
        $this->fileName = Str::uuid()->toString().self::SUFFIX;
        parent::__construct();
    }

    /**
     * @param array $record
     * @throws CannotReopenFile
     * @throws FailedToOpenFile
     */
    public function appendRecord(array $record): void
    {
        if ($this->isClosed) {
            throw new CannotReopenFile('[DWH] '.self::CANNOT_REOPEN_MESSAGE.' '.self::class);
        }

        if (!is_resource($this->fileHandler)) {
            $this->initializeFile($record);
        }

        foreach ($record as &$value) {
            if (is_null($value) || is_array($value)) {
                $value = '';
            }
        }
        fputcsv($this->fileHandler, array_values($record), self::DELIMITER);
    }

    /**
     * @param array $record
     * @throws FailedToOpenFile
     */
    private function initializeFile(array $record): void
    {
        $this->openFile();

        $keys = array_keys($record);
        foreach ($keys as &$key) {
            $key = strtolower((string) $key);
        }
        fputcsv($this->fileHandler, $keys, self::DELIMITER);
    }
}
