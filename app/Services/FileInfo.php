<?php

namespace App\Services;

class FileInfo
{
    private $disk;
    private $basePath;
    private $fileName;

    public function __construct(string $disk, string $basePath, string $fileName)
    {
        $this->disk = $disk;
        $this->basePath = $basePath;
        $this->fileName = $fileName;
    }

    public function path(): string
    {
        return $this->basePath.$this->fileName;
    }

    public function fileName(): string
    {
        return $this->fileName;
    }

    public function disk(): string
    {
        return $this->disk;
    }
}
