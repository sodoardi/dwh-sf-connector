<?php

namespace App\Console;

use App\Jobs\TransferSalesforceToDwh;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule
            ->command('queue:work mongodb --stop-when-empty')
            ->everyMinute()
            ->runInBackground();

        $fromDate = Carbon::yesterday()->format('Y-m-d');
        $toDate = Carbon::today()->format('Y-m-d');
        $schedule->job(new TransferSalesforceToDwh($fromDate, $toDate))
            ->dailyAt('02:00')
            ->environments(['production']);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
