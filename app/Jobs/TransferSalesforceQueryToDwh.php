<?php

namespace App\Jobs;

use Omniphx\Forrest\Providers\Laravel\Facades\Forrest;

class TransferSalesforceQueryToDwh extends TransferSalesforceResultsToDwh
{
    protected function getSalesforceResults(): array
    {
        Forrest::authenticate();
        return Forrest::query($this->query) ?? [];
    }
}
