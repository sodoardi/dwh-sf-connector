<?php

namespace App\Jobs;

use App\Services\FileInfo;
use App\Services\FileWriterFactory;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

abstract class TransferSalesforceResultsToDwh extends DwhJob
{
    protected const DWH_DISK = 'dwh';
    protected const WARNING_THRESHOLD = 0.8;
    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 600;
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * @var string
     */
    protected $query;
    /**
     * @var string
     */
    protected $reportDirectory;

    public function __construct(string $query, string $reportDirectory)
    {
        $this->query = $query;
        $this->reportDirectory = $reportDirectory;
    }

    public function handle(): void
    {
        $timeStart = microtime(true);

        $queryResult = $this->getSalesforceResults();

        if (!isset($queryResult['totalSize']) || $queryResult['totalSize'] === 0) {
            return;
        }

        $this->processResult($queryResult);

        $timeEnd = microtime(true);
        $executionTime = $timeEnd - $timeStart;

        if ($executionTime > self::WARNING_THRESHOLD * $this->timeout) {
            Log::warning('[DWH] Execution time exceeded '.(self::WARNING_THRESHOLD * 100).'% of defined timeout', [
                'labels' => 'DWH',
                'executionTime' => $executionTime,
                'timeout' => $this->timeout,
                'query' => $this->query,
            ]);
        }
    }

    abstract protected function getSalesforceResults(): array;

    private function processResult(array $queryResult): void
    {
        if (isset($queryResult['nextRecordsUrl'])) {
            TransferSalesforceNextUrlToDwh::dispatch($queryResult['nextRecordsUrl'], $this->reportDirectory);
        }

        $tmpFile = $this->prepareTmpFile($queryResult['records'] ?? []);
        $this->moveToDwh($tmpFile);
    }

    private function prepareTmpFile(array $records): FileInfo
    {
        $fileWriter = (new FileWriterFactory())->getFileWriter(config('dwh.file_mode'));
        $fileWriter->appendRecords($records);
        return $fileWriter->finishFile();
    }

    private function moveToDwh(FileInfo $fileInfo): void
    {
        $content = Storage::disk($fileInfo->disk())->get($fileInfo->path());
        Storage::disk(self::DWH_DISK)->makeDirectory($this->reportDirectory);
        Storage::disk(self::DWH_DISK)->put($this->reportDirectory . $fileInfo->fileName(), $content);
        Storage::disk($fileInfo->disk())->delete($fileInfo->path());
    }
}
