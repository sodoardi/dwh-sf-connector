<?php

namespace App\Jobs;

use Carbon\Carbon;
use Omniphx\Forrest\Providers\Laravel\Facades\Forrest;

class TransferSalesforceToDwh extends DwhJob
{
    private const DWH_REPORT = '/v1/dwh';
    private const MAX_PARALLEL_QUERIES = 5;
    private const QUERY_DELAY = 300;

    private $fromDate;
    private $toDate;

    /**
     * TransferSalesforceToDwh constructor.
     * @param string $fromDate
     * @param string $toDate
     */
    public function __construct($fromDate, $toDate)
    {
        $this->fromDate = $fromDate;
        $this->toDate = $toDate;
    }

    /**
     * Execute the job
     */
    public function handle(): void
    {
        $dateDirectory = Carbon::parse($this->toDate)->subDay()->format('Y/m/d/');

        Forrest::authenticate();
        $queries = Forrest::custom(self::DWH_REPORT . '?fromdate=' . $this->fromDate . '&todate=' . $this->toDate);

        $queryCount = 0;
        foreach ($queries as $reportName => $query) {
            $queryCount++;
            $delay = floor($queryCount / self::MAX_PARALLEL_QUERIES) * self::QUERY_DELAY;

            $reportName = str_replace('__c', '', strtolower($reportName));
            $reportDirectory = config('dwh.base_dir').$reportName.'/'.$dateDirectory;
            TransferSalesforceQueryToDwh::dispatch($query, $reportDirectory)
                ->delay((int) $delay);
        }
    }
}
