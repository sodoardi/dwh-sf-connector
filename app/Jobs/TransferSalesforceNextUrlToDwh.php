<?php

namespace App\Jobs;

use Omniphx\Forrest\Providers\Laravel\Facades\Forrest;

class TransferSalesforceNextUrlToDwh extends TransferSalesforceResultsToDwh
{
    protected function getSalesforceResults(): array
    {
        Forrest::authenticate();
        return Forrest::next($this->query) ?? [];
    }
}
