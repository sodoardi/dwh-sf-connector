<?php

namespace Tests\Unit\Jobs\DWH;

use App\Jobs\TransferSalesforceQueryToDwh;
use App\Jobs\TransferSalesforceToDwh;
use Carbon\Carbon;
use Illuminate\Support\Facades\Queue;
use Omniphx\Forrest\Providers\Laravel\Facades\Forrest;
use Tests\TestCase;

class TransferSalesforceToDwhTest extends TestCase
{
    public function test_get_and_forward_dwh_queries(): void
    {
        Queue::fake();
        $fromDate = Carbon::yesterday()->format('Y-m-d');
        $toDate = Carbon::today()->format('Y-m-d');

        Forrest::shouldReceive('authenticate');
        Forrest::shouldReceive('custom')->andReturn([
            'Opportunity' => 'SELECT Id FROM Opportunity'
        ]);

        TransferSalesforceToDwh::dispatchNow($fromDate, $toDate);

        Queue::assertPushedOn(config('queue.queues.dwh'), TransferSalesforceQueryToDwh::class);
    }

    public function test_get_and_forward_nothing(): void
    {
        Queue::fake();
        $fromDate = Carbon::yesterday()->format('Y-m-d');
        $toDate = Carbon::today()->format('Y-m-d');

        Forrest::shouldReceive('authenticate');
        Forrest::shouldReceive('custom')->andReturn([]);

        TransferSalesforceToDwh::dispatchNow($fromDate, $toDate);

        Queue::assertNothingPushed();
    }
}
