<?php

namespace Tests\Unit\Jobs\DWH;

use App\Jobs\TransferSalesforceNextUrlToDwh;
use App\Jobs\TransferSalesforceQueryToDwh;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;
use Omniphx\Forrest\Providers\Laravel\Facades\Forrest;
use Tests\TestCase;

class TransferSalesforceQueryToDwhTest extends TestCase
{
    public function test_uploads_query_results_to_dwh()
    {
        Queue::fake();
        Storage::fake('dwh');
        Storage::fake('local');

        $query = 'SELECT Id FROM Opportunity';
        $path = 'opportunity/2019/06/30/';

        $exampleMock = [
            'totalSize' => 1,
            'records' => [
                [
                    'attributes' => [
                    ],
                    'Id' => 'id',
                    'Amount' => null,
                ]
            ]
        ];

        Forrest::shouldReceive('authenticate');
        Forrest::shouldReceive('query')->with($query)->andReturn($exampleMock);

        TransferSalesforceQueryToDwh::dispatchNow($query, $path);

        $files = Storage::disk('dwh')->allFiles($path);
        self::assertCount(1, $files);

        $content = Storage::disk('dwh')->get($files[0]);
        self::assertEquals('[{"attributes":[],"Id":"id","Amount":null}]', $content);

        $localFiles = Storage::disk('local')->allFiles();
        self::assertCount(0, $localFiles);

        Queue::assertNothingPushed();
    }

    public function test_call_next_page()
    {
        Queue::fake();
        Storage::fake('dwh');
        Storage::fake('local');

        $query = 'SELECT Id FROM Opportunity';
        $path = 'opportunity/2019/06/30/';
        $nextUrl = '/nextResult';

        $nextPageExample = [
            'totalSize' => 2,
            'nextRecordsUrl' => $nextUrl,
            'records' => [
                [
                    'attributes' => [
                    ],
                    'Id' => 'id1',
                    'Amount' => null,
                ]
            ]
        ];

        Forrest::shouldReceive('authenticate');
        Forrest::shouldReceive('query')->with($query)->andReturn($nextPageExample);

        TransferSalesforceQueryToDwh::dispatchNow($query, $path);

        $files = Storage::disk('dwh')->allFiles($path);
        self::assertCount(1, $files);

        $content = Storage::disk('dwh')->get($files[0]);
        self::assertEquals('[{"attributes":[],"Id":"id1","Amount":null}]', $content);

        $localFiles = Storage::disk('local')->allFiles();
        self::assertCount(0, $localFiles);

        Queue::assertPushedOn(config('queue.queues.dwh'),TransferSalesforceNextUrlToDwh::class);
    }

    public function test_skips_empty_results()
    {
        Storage::fake('dwh');
        Storage::fake('local');

        $query = 'SELECT Id FROM Opportunity';
        $path = 'opportunity/2019/06/30/';

        $exampleMock = [
            'totalSize' => 0,
            'records' => []
        ];

        Forrest::shouldReceive('authenticate');
        Forrest::shouldReceive('query')->with($query)->andReturn($exampleMock);
        Log::shouldReceive('info')->withSomeOfArgs('[DWH] No results for query '.TransferSalesforceQueryToDwh::class);

        TransferSalesforceQueryToDwh::dispatchNow($query, $path);

        $files = Storage::disk('dwh')->allFiles($path);
        self::assertCount(0, $files);
    }
}
