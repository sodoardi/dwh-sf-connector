<?php

namespace Tests\Unit\Services;

use App\Exceptions\CannotReopenFile;
use App\Exceptions\NoFileContent;
use App\Services\CsvFileWriter;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class CsvFileWriterTest extends TestCase
{
    /**
     * @test
     * @see CsvFileWriter::appendRecord()
     * @param $record
     * @param $expectedContent
     * @dataProvider singleRecords
     */
    public function test_append_single_records($record, $expectedContent)
    {
        Storage::fake('local');

        $fileWriter = new CsvFileWriter();

        $fileWriter->appendRecord($record);
        $fileInfo = $fileWriter->finishFile();

        Storage::disk($fileInfo->disk())->assertExists($fileInfo->path());
        $fileHandler = fopen(Storage::disk($fileInfo->disk())->path($fileInfo->path()), 'r');
        for ($i = 0; $line = fgetcsv($fileHandler, 0, ';'); $i++) {
            self::assertEquals($expectedContent[$i],$line);
        }
    }

    public function singleRecords(): array
    {
        return [
            "record_with_values" => [
                [
                    'key' => 'value',
                    'key2' => 'value2'
                ],
                [
                    ["key", "key2"],
                    ["value", "value2"]
                ]
            ],
            "array_to_empty" => [
                [
                    'key' => 'value',
                    'key2' => []
                ],
                [
                    ["key", "key2"],
                    ["value", null] //php translates empty string to null :)
                ]
            ]
        ];
    }

    /**
     * @test
     * @see CsvFileWriter::appendRecords()
     */
    public function test_append_multiple_records()
    {
        Storage::fake('local');

        $fileWriter = new CsvFileWriter();
        $fileWriter->appendRecords(
            [
                [ 'key' => 'value' ],
                [ 'key' => [] ],
                [ 'key' => 'value2' ],
            ]
        );
        $fileInfo = $fileWriter->finishFile();

        Storage::disk($fileInfo->disk())->assertExists($fileInfo->path());
        $fileHandler = fopen(Storage::disk($fileInfo->disk())->path($fileInfo->path()), 'r');

        $expectedContent = [
            ['key'],
            ['value'],
            [null],
            ['value2'],
        ];

        for ($i = 0; $line = fgetcsv($fileHandler, 0, ';'); $i++) {
            self::assertEquals($expectedContent[$i],$line);
        }
    }

    public function test_fails_on_reopen()
    {
        Storage::fake('local');
        $fileWriter = new CsvFileWriter();
        $fileWriter->appendRecord(['key' => 'value']);
        $fileWriter->finishFile();

        self::expectException(CannotReopenFile::class);
        $fileWriter->appendRecord(['key' => 'value']);
    }

    public function test_fails_on_empty()
    {
        Storage::fake('local');
        $fileWriter = new CsvFileWriter();
        self::expectException(NoFileContent::class);
        $fileWriter->finishFile();
    }
}
