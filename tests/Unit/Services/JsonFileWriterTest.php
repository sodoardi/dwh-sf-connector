<?php

namespace Tests\Unit\Services;

use App\Exceptions\CannotReopenFile;
use App\Exceptions\NoFileContent;
use App\Services\JsonFileWriter;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class JsonFileWriterTest extends TestCase
{
    /**
     * @test
     * @see JsonFileWriter::appendRecord()
     * @param $record
     * @param $expectedContent
     * @dataProvider singleRecords
     */
    public function test_append_single_records($record, $expectedContent)
    {
        Storage::fake('local');

        $fileWriter = new JsonFileWriter();

        $fileWriter->appendRecord($record);
        $fileInfo = $fileWriter->finishFile();

        Storage::disk($fileInfo->disk())->assertExists($fileInfo->path());
        $content = Storage::disk($fileInfo->disk())->get($fileInfo->path());
        self::assertEquals($expectedContent, $content);
    }

    public function singleRecords(): array
    {
        return [
            "record_with_values" => [
                [
                    'key' => 'value'
                ],
                '[{"key":"value"}]'
            ],
//            deactivated as per AD-6112
//            "turns_null_to_string" => [
//                [
//                    'key' => null
//                ],
//                '[{"key":""}]'
//            ],
//            deactiveated as per AD-6237
//            "turns_array_to_null" => [
//                [
//                    'key' => []
//                ],
//                '[{"key":null}]'
//            ]
        ];
    }

    /**
     * @test
     * @see JsonFileWriter::appendRecords()
     */
    public function test_append_multiple_records()
    {
        Storage::fake('local');

        $fileWriter = new JsonFileWriter();
        $fileWriter->appendRecords(
            [
                [ 'key' => 'value' ],
                [ 'key' => null ],
                [ 'key' => [] ],
            ]
        );
        $fileInfo = $fileWriter->finishFile();

        Storage::disk($fileInfo->disk())->assertExists($fileInfo->path());
        $content = Storage::disk($fileInfo->disk())->get($fileInfo->path());
        self::assertEquals('[{"key":"value"},{"key":null},{"key":[]}]', $content);
    }

    public function test_fails_on_reopen()
    {
        Storage::fake('local');
        $fileWriter = new JsonFileWriter();
        $fileWriter->appendRecord(['key' => 'value']);
        $fileWriter->finishFile();

        self::expectException(CannotReopenFile::class);
        $fileWriter->appendRecord(['key' => 'value']);
    }

    public function test_fails_on_empty()
    {
        Storage::fake('local');
        $fileWriter = new JsonFileWriter();
        self::expectException(NoFileContent::class);
        $fileWriter->finishFile();
    }
}
