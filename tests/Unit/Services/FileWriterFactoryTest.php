<?php

namespace Tests\Unit\Services;

use App\Services\CsvFileWriter;
use App\Services\FileWriterFactory;
use App\Services\JsonFileWriter;
use InvalidArgumentException;
use Tests\TestCase;

class FileWriterFactoryTest extends TestCase
{
    /**
     * @test
     * @see FileWriterFactory::getFileWriter()
     * @param $mode
     * @param $fileWriter
     * @dataProvider fileWriters
     */
    public function test_generate_writers($mode, $fileWriter)
    {
        $fileWriterFactory = new FileWriterFactory();
        self::assertInstanceOf($fileWriter, $fileWriterFactory->getFileWriter($mode));
    }

    public function fileWriters(): array
    {
        return [
            'json_writer' => [
                'json',
                JsonFileWriter::class
            ],
            'csv_writer' => [
                'csv',
                CsvFileWriter::class
            ]
        ];
    }

    public function test_fails_invalid()
    {
        $fileWriterFactory = new FileWriterFactory();
        self::expectException(InvalidArgumentException::class);
        $fileWriterFactory->getFileWriter('not a valid mode');
    }
}
